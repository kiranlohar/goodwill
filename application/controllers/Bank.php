<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Bank_model', 'BANK', TRUE); 
    }
	
	public function index()
	{ 

	  $banks = $this->BANK->getList();	
	  $data['banks'] = $banks;
	  $data['page_title']= "Bank:Master";
	  $data['page']= "bank/index";
      $this->load->view('components/container', $data);
	}



	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){

			$bank_name = $this->input->post('bank_name');

			if(!$bank_name){
				$err[] = "Bank is Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'bank_name' => $bank_name,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1

								);
				
				if($this->BANK->insert($dbOject)){
					redirect(base_url('bank'));
				}else{
					echo 'failed';
				}
			}

		}
	}


	public function getBank()
	{
		$bank_id = $this->input->post('id');
		if(!$bank_id){
			$err[] = "Bank id not provided";
		}

		if(empty($err)){ 
			$bank = $this->BANK->getRecord($bank_id);
			if($bank){
				 echo json_encode(array('status'=>1,'data'=>$bank));
			}else{
				echo 'failed';
				 
			}

		}
	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){

			$bank = $this->input->post('edit_bank_name');
			$bank_id = $this->input->post('edit_id');

			if(!$bank_id){
				$err[] = "Bank id not provied";
			}
			if(!$bank){
				$err[] = "Bank Not provided";
			}


			if(empty($err)){
				$dbOject = array(

								'bank_name' => $bank, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->BANK->update($bank_id,$dbOject)){
					redirect(base_url('bank'));
				}else{
					echo 'failed';
				}
			}
		}
	}


	public function delete()
	{

		$err =array();
		if($this->input->post('doDelete')){
 
			$bank_id = $this->input->post('delete_id');

			if(!$bank_id){
				$err[] = "Bank id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->BANK->update($bank_id, $dbOject)){
					redirect(base_url('bank'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}


}
