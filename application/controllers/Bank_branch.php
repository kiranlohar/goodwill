<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_branch extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Bank_branch_model', 'BANKBRANCH', TRUE); 
       	$this->load->model ('Bank_model', 'BANK', TRUE); 
    }
	
	public function index()
	{ 
	  $banks_branch = $this->BANKBRANCH->getList();	
	  $banks = $this->BANK->getList();
	  $data['banks'] = $banks;
	  $data['banks_branch'] = $banks_branch;
	  $data['page_title']= "Bank_branch:Master";
	  $data['page']= "bank_branch/index";
      $this->load->view('components/container', $data);
	}

	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){
			$bank_id = $this->input->post('bank_id');
			$ifsc_code = $this->input->post('ifsc_code');
			$branch_name = $this->input->post('branch_name');
			$branch_address = $this->input->post('branch_address');

			if(!$bank_id){
				$err[] = "Bank is Not provided";
			}

			if(empty($err)){
				$dbOject = array(
								'bank_id' => $bank_id,
								'ifsc_code' => $ifsc_code,
								'branch_name' => $branch_name,
								'branch_address' => $branch_address,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1
								);
				if($this->BANKBRANCH->insert($dbOject)){
					redirect(base_url('Bank_branch'));
				}else{
					echo 'failed';
				}
			}
		}
	}

	public function getBank_branch()
	{
		$bank_id = $this->input->post('id');
		if(!$bank_id){
			$err[] = "Bank branch id is not provided";
		}

		if(empty($err)){ 
			$bank = $this->BANKBRANCH->getRecord($bank_id);
			if($bank){
				 echo json_encode(array('status'=>1,'data'=>$bank));
			}else{
				echo 'failed';
				 
			}

		}
	}

	public function edit()
	{
		//die("okkkk");
		$err =array();
		if($this->input->post('doEdit')){

			$edit_bank_branch_id = $this->input->post('edit_bank_id');
			$edit_ifsc_code = $this->input->post('edit_bank_ifsc_code');
			$edit_bank_branch_name = $this->input->post('edit_bank_branch_name');
			$edit_bank_branch_address = $this->input->post('edit_bank_branch_address');
			$bank_branch_id = $this->input->post('edit_id');
	
			//print_r($this->input->post());die;

			if(!$edit_bank_branch_id){
				$err[] = "Bank branch id not provied";
			}
			if(!$edit_ifsc_code){
				$err[] = "Bank branch IFSC code is Not provided";
			}


			if(empty($err)){
				$dbOject = array(

								'bank_id' => $edit_bank_branch_id, 
								'ifsc_code' => $edit_ifsc_code, 
								'branch_name' => $edit_bank_branch_name, 
								'branch_address' => $edit_bank_branch_address, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->BANKBRANCH->update($bank_branch_id,$dbOject)){
					redirect(base_url('bank_branch'));
				}else{
					echo 'failed';
				}
			}
		}
		else{
			print_r($err);
		}
	}


	public function delete()
	{

		$err =array();
		if($this->input->post('doDelete')){
 
			$bank_branch_id = $this->input->post('delete_id');

			if(!$bank_branch_id){
				$err[] = "Bank Branch id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->BANKBRANCH->update($bank_branch_id, $dbOject)){
					redirect(base_url('Bank_branch'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}


	public function getAjaxBranch()
	{
		$bank_id = $this->input->post('bank_id');
		$branhData = $this->BANKBRANCH->getBankBranchByBank($bank_id);
		echo json_encode($branhData);
	}

}
