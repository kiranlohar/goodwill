<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('State_model', 'STATE', TRUE); 
   		$this->load->model ('Country_model', 'COUNTRY', TRUE); 
   		$this->load->model ('City_model', 'CITY', TRUE); 
    }
	
	public function index()
	{ 
	  // $cities = $this->CITY->getCityStateCountryByCityId();
	  $cities = $this->CITY->getList();
	  // echo json_encode($cities);exit;
	  $countries = $this->COUNTRY->getList();	
	  $states = $this->STATE->getList();
	  $data['cities'] = $cities;
	  $data['countries'] = $countries;
	  $data['states'] = $states;
	  $data['page_title']= "City:Master";
	  $data['page']= "city/index";
      $this->load->view('components/container', $data);
	}

	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){


			$country_id = $this->input->post('country_id');
			$state_id = $this->input->post('state_id'); 
			$city_name = $this->input->post('name'); 

			if(!$country_id){
				$err[] = "country id Not provided";
			}
			if(!$state_id){
				$err[] = "City name Not provided";
			}
			if(!$city_name){
							$err[] = "City name Not provided";
						}

			if(empty($err)){
				$dbOject = array(

								'state_id' => $state_id,
								'name' => $city_name,  
								'isActive' => 1,  

								);
				
				if($this->CITY->insert($dbOject)){
					redirect(base_url('city'));
				}else{
					echo 'failed';
				}
			}

		}
	}


	public function getCity()
	{
		$city_id = $this->input->post('id');
		if(!$city_id){
			$err[] = "City id not provided";
		}

		if(empty($err)){ 
			$city = $this->CITY->getRecord($city_id);
			$state = $this->STATE->getRecord($city['state_id']);
			$city['country_id'] = $state['country_id'];
			if($city){
				 echo json_encode(array('status'=>1,'data'=>$city));
			}else{
				echo 'failed';
			}
		}


	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){

			$city_id = $this->input->post('edit_id');
			$city_name = $this->input->post('edit_city_name'); 
		//	$edit_state_id = $this->input->post('edit_state_id'); 
			//print_r($this->input->post());die;

			if(!$city_id){
				$err[] = "city id Not provided";
			}

			if(!$city_name){
				$err[] = "state name Not provided";
			}
			if(empty($err)){
				$dbOject = array(	  
								'name' => $city_name,  
								'isActive' => 1

								);
				
				if($this->CITY->update($city_id,$dbOject)){
					redirect(base_url('city'));
				}else{
					echo 'failed';
				}
			}
		}
	}


	public function delete()
	{
		$err =array();
		if($this->input->post('doDelete')){ 
			$state_id = $this->input->post('delete_id');
			if(!$state_id){
				$err[] = "State id not provied";
			} 
			if(empty($err)){
				$dbOject = array(
								'isActive' => 0   
								);
				if($this->CITY->update($state_id, $dbOject)){
					redirect(base_url('city'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}


public function getAjaxCity()
	{
		$state_id = $this->input->post('state_id');
		$cityData = $this->CITY->getCityByState($state_id);
		echo json_encode($cityData);
	}

}
