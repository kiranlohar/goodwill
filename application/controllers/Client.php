<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Client_model', 'CLIENT', TRUE); 
       	$this->load->model ('Bank_branch_model', 'BANKBRANCH', TRUE); 
       	$this->load->model ('Bank_model', 'BANK', TRUE); 
       	$this->load->model ('Country_model', 'COUNTRY', TRUE);
       	$this->load->model ('State_model', 'STATE', TRUE);
       	$this->load->model ('City_model', 'CITY', TRUE);
    }
	
	public function index()
	{
	  $countries = $this->COUNTRY->getList();	
	  $data['countries'] = $countries;
	  $cities = $this->CITY->getList();	
	  $data['cities'] = $cities; 
	  $states = $this->STATE->getList();	
	  $data['states'] = $states; 
	  $clients = $this->CLIENT->getList();
	  $data['clients'] = $clients;
	  $data['page_title']= "Client_branch:Master";
	  $data['page']= "Client/index";
      $this->load->view('components/container', $data);
	}

	public function add()
	{	
		//print_r($this->input->post());die;

		$err =array();
		if($this->input->post('doSubmit')){
			$client_type = $this->input->post('client_type');
			$client_name = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$mobile_number = $this->input->post('mobile_number');
			$adhar_number = $this->input->post('adhar_number');
			$pan_card_number = $this->input->post('pan_card_number');
			$birth_date = $this->input->post('birth_date');
			$address = $this->input->post('address');
			$country_id = $this->input->post('country_id');
			$state_id = $this->input->post('state_id');
			$city_id = $this->input->post('city_id');

			if(!$client_name){
				$err[] = "Client Name is Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'client_type' => $client_type,
								'name' => $client_name,
								'phone_number' => $phone_number,
								'mobile_number' => $mobile_number,
								'adhar_number' => $adhar_number,
								'pan_card_number' => $pan_card_number,
								'birth_date' => $birth_date,
								'address' => $address,
								'country_id' => $country_id,
								'state_id' => $state_id,
								'city_id' => $city_id,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1
								);
				if($this->CLIENT->insert($dbOject)){
					redirect(base_url('client'));
				}else{
					echo 'failed';
				}
			}
		}
	}

	public function getClient()
	{
		$bank_id = $this->input->post('id');
		if(!$bank_id){
			$err[] = "Bank branch id is not provided";
		}

		if(empty($err)){ 
			$bank = $this->CLIENT->getRecord($bank_id);
			if($bank){
				 echo json_encode(array('status'=>1,'data'=>$bank));
			}else{
				echo 'failed';
				 
			}

		}
	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){
			//print_r($this->input->post());die;
			$client_id = $this->input->post('edit_id');
			$edit_client_name = $this->input->post('edit_name');
			$edit_client_type = $this->input->post('edit_client_type');
			$edit_phone_number = $this->input->post('edit_phone_number');
			$edit_mobile_number = $this->input->post('edit_mobile_number');
			$edit_adhar_number = $this->input->post('edit_adhar_number');
			$edit_pan_card_number = $this->input->post('edit_pan_card_number');
			$edit_birth_date = $this->input->post('edit_birth_date');
			$edit_address = $this->input->post('edit_address');
			$edit_country_id = $this->input->post('edit_country_id');
			$edit_state_id = $this->input->post('edit_state_id');
			$edit_city_id = $this->input->post('edit_city_id');
	
			if(!$client_id){
				$err[] = "Client id not provied";
			}
			if(!$edit_client_name){
				$err[] = "Client Name is Not provided";
			}


			if(empty($err)){
				$dbOject = array(
								'name' => $edit_client_name,
								'client_type' => $edit_client_type,
								'phone_number' => $edit_phone_number,
								'mobile_number' => $edit_mobile_number,
								'adhar_number' => $edit_adhar_number,
								'pan_card_number' => $edit_pan_card_number,
								'birth_date' => $edit_birth_date,
								'address' => $edit_address,
								'country_id' => $edit_country_id,
								'state_id' => $edit_state_id,
								'city_id' => $edit_city_id,
								'updated_at' => date("Y-m-d H:i:s"),
								'updated_by' => 1
								);
				
				if($this->CLIENT->update($client_id,$dbOject)){
					redirect(base_url('client'));
				}else{
					echo 'failed';
				}
			}
			else{
				print_r($err);
			}
		}
		else{
			print_r($err);
		}
	}


	public function delete()
	{

		$err =array();
		if($this->input->post('doDelete')){
 
			$bank_branch_id = $this->input->post('delete_id');

			if(!$bank_branch_id){
				$err[] = "Bank Branch id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->CLIENT->update($bank_branch_id, $dbOject)){
					redirect(base_url('Client'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}


}
