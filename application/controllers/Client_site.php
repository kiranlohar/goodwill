<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_site extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Client_site_model', 'CLIENTSITE', TRUE); 
       	$this->load->model ('Client_model', 'CLIENT', TRUE); 
       	$this->load->model ('Country_model', 'COUNTRY', TRUE);
       	$this->load->model ('State_model', 'STATE', TRUE);
       	$this->load->model ('City_model', 'CITY', TRUE);
    }
	

	
	public function index()
	{
	  $countries = $this->COUNTRY->getList();	
	  $data['countries'] = $countries;	
	  $client_sites = $this->CLIENTSITE->getSitesWithClientNameList();	
	  $data['client_sites'] = $client_sites;
	  $cities = $this->CITY->getList();	
	  $data['cities'] = $cities; 
	  $states = $this->STATE->getList();	
	  $data['states'] = $states; 
	  $clients = $this->CLIENT->getList();
	  $data['clients'] = $clients;
	  $data['page_title']= "Client_site:Master";
	  $data['page']= "Client_site/index";
      $this->load->view('components/container', $data);
	}

	public function add()
	{	
		//print_r($this->input->post());die;

		$err =array();
		if($this->input->post('doSubmit')){
			$client_id = $this->input->post('client_id');
			$site_name = $this->input->post('site_name');
			$site_address = $this->input->post('site_address');
			$country_id = $this->input->post('country_id');
			$state_id = $this->input->post('state_id');
			$city_id = $this->input->post('city_id');

			if(!$client_id){
				$err[] = "Client ID Name is Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'client_id' => $client_id,
								'site_name' => $site_name,
								'site_address' => $site_address,
								'country_id' => $country_id,
								'state_id' => $state_id,
								'city_id' => $city_id,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1
								);
				if($this->CLIENTSITE->insert($dbOject)){
					redirect(base_url('Client_site'));
				}else{
					echo 'failed';
				}
			}
		}
	}

	public function getClient_site()
	{
		$client_site_id = $this->input->post('id');
		if(!$client_site_id){
			$err[] = "Bank branch id is not provided";
		}

		if(empty($err)){ 
			$bank = $this->CLIENTSITE->getRecord($client_site_id);
			if($bank){
				 echo json_encode(array('status'=>1,'data'=>$bank));
			}else{
				echo 'failed';
				 
			}

		}
	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){
			// print_r($this->input->post());die;
			$client_id = $this->input->post('edit_id');
			$edit_client_id = $this->input->post('edit_client_id');
			$edit_site_name = $this->input->post('edit_site_name');
			$edit_site_address = $this->input->post('edit_site_address');
			$edit_country_id = $this->input->post('edit_country_id');
			$edit_state_id = $this->input->post('edit_state_id');
			$edit_city_id = $this->input->post('edit_city_id');
	
			if(!$client_id){
				$err[] = "Client id not provied";
			}
			if(!$edit_site_name){
				$err[] = "Client Name is Not provided";
			}


			if(empty($err)){
				$dbOject = array(
								'client_id' => $edit_client_id,
								'site_name' => $edit_site_name,
								'site_address' => $edit_site_address,
								'country_id' => $edit_country_id,
								'state_id' => $edit_state_id,
								'city_id' => $edit_city_id,
								'updated_at' => date("Y-m-d H:i:s"),
								'updated_by' => 1
								);
				
				if($this->CLIENTSITE->update($client_id,$dbOject)){
					redirect(base_url('Client_site'));
				}else{
					echo 'failed';
				}
			}
			else{
				print_r($err);
			}
		}
		else{
			print_r($err);
		}
	}


	public function delete()
	{
		$err =array();
		if($this->input->post('doDelete')){
 
			$client_site_id = $this->input->post('delete_id');

			if(!$client_site_id){
				$err[] = "Client site id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1
								);
				
				if($this->CLIENTSITE->update($client_site_id, $dbOject)){
					redirect(base_url('Client_site'));
				}else{
					echo 'failed';
				}
			}
		}
	}


}
