<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Country_model', 'COUNTRY', TRUE); 
    }
	
	public function index()
	{ 
	  $countries = $this->COUNTRY->getList();	
	  $data['countries'] = $countries;
	  $data['page_title']= "Country:Master";
	  $data['page']= "country/index";
      $this->load->view('components/container', $data);
	}


	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){

			$country_short_name = $this->input->post('country_short_name');
			$country_name = $this->input->post('country_name');
			$country_code = $this->input->post('country_code');

			if(!$country_short_name){
				$err[] = "country short name Not provided";
			}
			if(!$country_name){
				$err[] = "country name Not provided";
			}
			if(!$country_code){
				$err[] = "country code Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'sortname' => $country_short_name, 
								'name' => $country_name, 
								'phonecode' => $country_code
								);
				
				if($this->COUNTRY->insert($dbOject)){
					redirect(base_url('country'));
				}else{
					echo 'failed';
				}
			}

		}
	}


	public function getCountry()
	{
		$country_id = $this->input->post('id');
		if(!$country_id){
			$err[] = "Designation id not provied";
		}

		if(empty($err)){ 
			$country = $this->COUNTRY->getRecord($country_id);
			if($country){
				 echo json_encode(array('status'=>1,'data'=>$country));
			}else{
				echo 'failed';
			}
		}


	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){

			$country_short_name = $this->input->post('edit_country_short_name');
			$country_name = $this->input->post('edit_country_name');
			$country_code = $this->input->post('edit_country_code');
			$country_id = $this->input->post('edit_id');

			if(!$country_id){
				$err[] = "Country id not provied";
			}
			if(!$country_short_name){
				$err[] = "country short name Not provided";
			}
			if(!$country_name){
				$err[] = "country name Not provided";
			}
			if(!$country_code){
				$err[] = "country code Not provided";
			}

			if(empty($err)){
				$dbOject = array(
								'sortname' => $country_short_name, 
								'name' => $country_name, 
								'phonecode' => $country_code,
								'isActive' => 1
								);
				
				if($this->COUNTRY->update($country_id,$dbOject)){
					redirect(base_url('country'));
				}else{
					echo 'failed';
				}
			}
		}
	}


	public function delete()
	{
		$err =array();
		if($this->input->post('doDelete')){ 
			$country_id = $this->input->post('delete_id');
			if(!$country_id){
				$err[] = "Country id not provied";
			} 
			if(empty($err)){
				$dbOject = array(
								'isActive' => 0
								);				
				if($this->COUNTRY->update($country_id, $dbOject)){
					redirect(base_url('country'));
				}else{
					echo 'failed';
				}
			}			 
		}			 
	}
}
