<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Department_model', 'DEPARTMENT', TRUE); 
    }
	
	public function index()
	{ 

	  $departments = $this->DEPARTMENT->getList();	
	  $data['departments'] = $departments;
	  $data['page_title']= "Department:Master";
	  $data['page']= "department/index";
      $this->load->view('components/container', $data);
	}



	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){

			$department_name = $this->input->post('department_name');

			if(!$department_name){
				$err[] = "Department Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'department_name' => $department_name,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1

								);
				
				if($this->DEPARTMENT->insert($dbOject)){
					redirect(base_url('department'));
				}else{
					echo 'failed';
				}
			}

		}
	}


	public function getDepartment()
	{
		$department_id = $this->input->post('id');
		if(!$department_id){
			$err[] = "Department id not provided";
		}

		if(empty($err)){ 
			$department = $this->DEPARTMENT->getRecord($department_id);
			if($department){
				 echo json_encode(array('status'=>1,'data'=>$department));
			}else{
				echo 'failed';
				 
			}

		}
	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){

			$department = $this->input->post('edit_department_name');
			$department_id = $this->input->post('edit_id');

			if(!$department_id){
				$err[] = "Department id not provied";
			}
			if(!$department){
				$err[] = "Department Not provided";
			}


			if(empty($err)){
				$dbOject = array(

								'department_name' => $department, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->DEPARTMENT->update($department_id,$dbOject)){
					redirect(base_url('department'));
				}else{
					echo 'failed';
				}
			}
		}
	}


	public function delete()
	{

		$err =array();
		if($this->input->post('doDelete')){
 
			$department_id = $this->input->post('delete_id');

			if(!$department_id){
				$err[] = "Department id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->DEPARTMENT->update($department_id, $dbOject)){
					redirect(base_url('department'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}


}
