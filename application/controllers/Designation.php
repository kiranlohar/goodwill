<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designation extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Designation_model', 'DESIGNATION', TRUE); 
    }
	
	public function index()
	{ 

	  $designations = $this->DESIGNATION->getList();	
	  $data['designations'] = $designations;
	  $data['page_title']= "Designation:Master";
	  $data['page']= "designation/index";
      $this->load->view('components/container', $data);
	}



	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){

			$designation = $this->input->post('designation');

			if(!$designation){
				$err[] = "Designation Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'designation' => $designation,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1

								);
				
				if($this->DESIGNATION->insert($dbOject)){
					redirect(base_url('designation'));
				}else{
					echo 'failed';
				}
			}

		}
	}


	public function getDesignation()
	{
		$designation_id = $this->input->post('id');
		if(!$designation_id){
			$err[] = "Designation id not provied";
		}

		if(empty($err)){ 
			$designation = $this->DESIGNATION->getRecord($designation_id);
			if($designation){
				 echo json_encode(array('status'=>1,'data'=>$designation));
			}else{
				echo 'failed';
			}
		}


	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){

			$designation = $this->input->post('edit_designation');
			$designation_id = $this->input->post('edit_id');

			if(!$designation_id){
				$err[] = "Designation id not provied";
			}
			if(!$designation){
				$err[] = "Designation Not provided";
			}


			if(empty($err)){
				$dbOject = array(

								'designation' => $designation, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->DESIGNATION->update($designation_id,$dbOject)){
					redirect(base_url('designation'));
				}else{
					echo 'failed';
				}
			}
		}
	}


	public function delete()
	{

		$err =array();
		if($this->input->post('doDelete')){
 
			$designation_id = $this->input->post('delete_id');

			if(!$designation_id){
				$err[] = "Designation id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->DESIGNATION->update($designation_id, $dbOject)){
					redirect(base_url('designation'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}


}
