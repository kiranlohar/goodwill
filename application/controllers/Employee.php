<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Employee_model', 'EMPLOYEE', TRUE); 
       	$this->load->model ('Country_model', 'COUNTRY', TRUE);
       	$this->load->model ('Bank_model', 'BANK', TRUE);
       	$this->load->model ('Bank_branch_model', 'BANKBRANCH', TRUE); 
       	$this->load->model ('Department_model', 'DEPARTMENT', TRUE);
       	$this->load->model ('Qualification_model', 'QUALIFICATION', TRUE); 
       	$this->load->model ('Designation_model', 'DESIGNATION', TRUE); 
       	$this->load->model ('Gender_model', 'GENDER', TRUE); 
       	$this->load->model ('Marital_status_model', 'MARIRALSTATUS', TRUE); 

       	$this->load->model ('State_model', 'STATE', TRUE);  
   		$this->load->model ('City_model', 'CITY', TRUE); 


    }
	
	public function index()
	{
	  $employee_details = $this->EMPLOYEE->getList();	
	  $countries = $this->COUNTRY->getList();	
	  $banks = $this->BANK->getList();	
	  $departments = $this->DEPARTMENT->getList();	
	  $qualifications = $this->QUALIFICATION->getList();
	  $designations = $this->DESIGNATION->getList();	
	  $genders = $this->GENDER->getList();	
	  $marital_status = $this->MARIRALSTATUS->getList();	


	  $data['employee_details'] = $employee_details;	 
	  $data['countries'] = $countries;	 
	  $data['banks'] = $banks;	 
	  $data['departments'] = $departments;	 
	  $data['qualifications'] = $qualifications;	 
	  $data['designations'] = $designations;	 
	  $data['genders'] = $genders;	 
	  $data['marital_status'] = $marital_status;	 



	  $data['page_title']= "Employee:Master";
	  $data['page']= "employee/index";
      $this->load->view('components/container', $data);
	}

	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){


			// echo '<pre>';
			// print_r($this->input->post());exit();

			$employee_code = $this->input->post('employee_code');
			$gender = $this->input->post('gender');
			$name = $this->input->post('name');
			$relation = $this->input->post('relation');
			$relation_name = $this->input->post('relation_name');
			$phone_number = $this->input->post('phone_number');
			$mobile_number = $this->input->post('mobile_number');
			$aadhar_number = $this->input->post('aadhar_number');
			$pan_card_number = $this->input->post('pan_card_number');
			$birth_date = $this->input->post('birth_date');
			$joining_date = $this->input->post('joining_date');
			$releaving_date = $this->input->post('releaving_date');
			$address = $this->input->post('address');
			$country_id = $this->input->post('country_id');
			$state_id = $this->input->post('state_id');
			$city_id = $this->input->post('city_id');
			$bank_id = $this->input->post('bank_id');
			$bank_branch_id = $this->input->post('bank_branch_id');
			$account_number = $this->input->post('account_number');
			$ifsc_code = $this->input->post('ifsc_code');
			$esi_reg_number = $this->input->post('esi_reg_number');
			$pf_reg_number = $this->input->post('pf_reg_number');
			$uan = $this->input->post('uan');
			$qualification_id = $this->input->post('qualification_id');
			$employee_type = $this->input->post('employee_type');
			$designation_id = $this->input->post('designation_id');
			$email_id = $this->input->post('email_id');
			$marital_status_id = $this->input->post('marital_status_id');
			$nationality = $this->input->post('nationality');
			$date_of_epf = $this->input->post('date_of_epf');
			$date_of_pension = $this->input->post('date_of_pension');
			$is_handicap = $this->input->post('is_handicap');
			$handicap_type = $this->input->post('handicap_type');
			$next_of_kin = $this->input->post('next_of_kin');
 
			if(!$employee_code){
				$err[] = "Employee code Not provided";
			}
			if(!$gender){
				$err[] = "Gender Not provided";
			}
			if(!$name){
				$err[] = "Name Not provided";
			}
			if(!$relation){
				$err[] = "Relation Not provided";
			}
			if(!$relation_name){
				$err[] = "Relation Name Not provided";
			} 
			if(!$phone_number){
				$err[] = "Phone Number is Not provided";
			} 
			if(!$mobile_number){
				$err[] = "Mobile Number Not provided";
			} 
			if(!$pan_card_number){
				$err[] = "Pan card number is Not provided";
			} 
			if(!$birth_date){
				$err[] = "Birth date is Not provided";
			} 
			if(!$joining_date){
				$err[] = "Joining date is Not provided";
			} 
			if(!$releaving_date){
				$err[] = "Releving Date is Not provided";
			} 
			if(!$address){
				$err[] = "Address is Not provided";
			} 
			if(!$country_id){
				$err[] = "Country is Not provided";
			} 
			if(!$state_id){
				$err[] = "State is Not provided";
			} 
			if(!$city_id){
				$err[] = "City is Not provided";
			} 
			if(!$bank_id){
				$err[] = "Bank is Not provided";
			} 
			if(!$bank_branch_id){
				$err[] = "Bank Branch is Not provided";
			} 
			if(!$account_number){
				$err[] = "Account is Not provided";
			} 
			if(!$ifsc_code){
				$err[] = "IFSC is Not provided";
			} 
			if(!$esi_reg_number){
				$err[] = "ESI Registration Number is Not provided";
			}
			if(!$pf_reg_number){
				$err[] = "PF Registration Number is Not provided";
			} 
			if(!$uan){
				$err[] = "PF Registration Number is Not provided";
			} 
			if(!$qualification_id){
				$err[] = "PF Registration Number is Not provided";
			}
			if(!$employee_type){
				$err[] = "PF Registration Number is Not provided";
			} 
			if(!$designation_id){
				$err[] = "PF Registration Number is Not provided";
			}
			if(!$email_id){
				$err[] = "email_id-ID is Not provided";
			} 
			if(!$marital_status_id){
				$err[] = "Marital status is Not provided";
			} 
			if(!$nationality){
				$err[] = "Nationality is Not provided";
			} 
			if(!$date_of_epf){
				$err[] = "Date of Epf is Not provided";
			} 
			if(!$date_of_pension){
				$err[] = "Date of Pension is Not provided";
			} 
			if(!$is_handicap){
				$err[] = "Handicap is Not provided";
			}  
			if(!$handicap_type){
				$err[] = "Handicap Type is Not provided";
			}
			if(!$next_of_kin){
				$err[] = "next of kin Type is Not provided";
			} 
 
			if(empty($err)){
				$dbOject = array(
 								'employee_code' => $employee_code,
								'gender_id' => $gender,
								'name' => $name,
								'relation' => $relation,
								'relation_name' => $relation_name,
								'phone_number' => $phone_number,
								'mobile_number' => $mobile_number,
								'adhar_number' => $aadhar_number,
								'pan_card_number' => $pan_card_number,
								'birth_date' => $birth_date,
								'joining_date' => $joining_date,
								'releaving_date' => $releaving_date,
								'address' => $address,
								'country_id' => $country_id,
								'state_id' => $state_id,
								'city_id' => $city_id,
								'bank_id' => $bank_id,
								'bank_branch_id' => $bank_branch_id,
								'account_number' => $account_number,
								'ifsc_code' => $ifsc_code,
								'esi_reg_number' => $esi_reg_number,
								'pf_reg_number' => $pf_reg_number,
								'uan' => $uan,
								'qualification_id' => $qualification_id,
								'employee_type' => $employee_type,
								'designation_id' => $designation_id,
								'email_id' => $email_id,
								'marital_status_id' => $marital_status_id,
								'nationality' => $nationality,
								'date_of_epf' => $date_of_epf,
								'date_of_pension' => $date_of_pension,
								'is_handicap' => $is_handicap,
								'handicap_type' => $handicap_type,
								'next_of_kin' => $next_of_kin,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1
								);
				if($this->EMPLOYEE->insert($dbOject)){
					redirect(base_url('employee'));
				}else{
					echo 'failed';
				}
			}
		}
	}

	public function getEmployeeDetails()
	{
		$employee_id = $this->input->post('id');
		if(!$employee_id){
			$err[] = "employee id is not provided";
		}

		if(empty($err)){ 
			$employee_detail = $this->EMPLOYEE->getRecord($employee_id);
			// $states = $this->STATE->getStateByCountry($employee_detail['country_id']);
			// $cities = $this->CITY->getCityByState($employee_detail['state_id']);

			// $employee_detail['states'] = $states;
			// $employee_detail['cities'] = $cities;

			if($employee_detail){
				 echo json_encode(array('status'=>1,'data'=>$employee_detail));
			}else{
				echo 'failed';
				 
			}

		}
	}

	public function edit()
	{
		//die("okkkk");
		$err =array();
		if($this->input->post('doEdit')){

			$id = $this->input->post('edit_id');
			$employee_code = $this->input->post('edit_employee_code');
			$gender = $this->input->post('edit_gender_id');
			$name = $this->input->post('edit_name');
			$relation = $this->input->post('edit_relation');
			$relation_name = $this->input->post('edit_relation_name');
			$phone_number = $this->input->post('edit_phone_number');
			$mobile_number = $this->input->post('edit_mobile_number');
			$aadhar_number = $this->input->post('edit_aadhar_number');
			$pan_card_number = $this->input->post('edit_pan_card_number');
			$birth_date = $this->input->post('edit_birth_date');
			$joining_date = $this->input->post('edit_joining_date');
			$releaving_date = $this->input->post('edit_releaving_date');
			$address = $this->input->post('edit_address');
			$country_id = $this->input->post('edit_country_id');
			$state_id = $this->input->post('edit_state_id');
			$city_id = $this->input->post('edit_city_id');
			$bank_id = $this->input->post('edit_bank_id');
			$bank_branch_id = $this->input->post('edit_bank_branch_id');
			$account_number = $this->input->post('edit_account_number');
			$ifsc_code = $this->input->post('edit_ifsc_code');
			$esi_reg_number = $this->input->post('edit_esi_reg_number');
			$pf_reg_number = $this->input->post('edit_pf_reg_number');
			$uan = $this->input->post('edit_uan');
			$qualification_id = $this->input->post('edit_qualification_id');
			$employee_type = $this->input->post('edit_employee_type');
			$designation_id = $this->input->post('edit_designation_id');
			$email_id = $this->input->post('edit_email_id');
			$marital_status_id = $this->input->post('edit_marital_status_id');
			$nationality = $this->input->post('edit_nationality');
			$date_of_epf = $this->input->post('edit_date_of_epf');
			$date_of_pension = $this->input->post('edit_date_of_pension');
			$is_handicap = $this->input->post('edit_is_handicap');
			$handicap_type = $this->input->post('edit_handicap_type');
			$next_of_kin = $this->input->post('edit_next_of_kin');

			if(!$id){
				$err[] = "Employee id Not provided";
			}
			if(!$employee_code){
				$err[] = "Employee code Not provided";
			}
			if(!$gender){
				$err[] = "Gender Not provided";
			}
			if(!$name){
				$err[] = "Name Not provided";
			}
			if(!$relation){
				$err[] = "Relation Not provided";
			}
			if(!$relation_name){
				$err[] = "Relation Name Not provided";
			} 
			if(!$phone_number){
				$err[] = "Phone Number is Not provided";
			} 
			if(!$mobile_number){
				$err[] = "Mobile Number Not provided";
			} 
			if(!$pan_card_number){
				$err[] = "Pan card number is Not provided";
			} 
			if(!$birth_date){
				$err[] = "Birth date is Not provided";
			} 
			if(!$joining_date){
				$err[] = "Joining date is Not provided";
			} 
			if(!$releaving_date){
				$err[] = "Releving Date is Not provided";
			} 
			if(!$address){
				$err[] = "Address is Not provided";
			} 
			if(!$country_id){
				$err[] = "Country is Not provided";
			} 
			if(!$state_id){
				$err[] = "State is Not provided";
			} 
			if(!$city_id){
				$err[] = "City is Not provided";
			} 
			if(!$bank_id){
				$err[] = "Bank is Not provided";
			} 
			if(!$bank_branch_id){
				$err[] = "Bank Branch is Not provided";
			} 
			if(!$account_number){
				$err[] = "Account is Not provided";
			} 
			if(!$ifsc_code){
				$err[] = "IFSC is Not provided";
			} 
			if(!$esi_reg_number){
				$err[] = "ESI Registration Number is Not provided";
			}
			if(!$pf_reg_number){
				$err[] = "PF Registration Number is Not provided";
			} 
			if(!$uan){
				$err[] = "PF Registration Number is Not provided";
			} 
			if(!$qualification_id){
				$err[] = "PF Registration Number is Not provided";
			}
			if(!$employee_type){
				$err[] = "PF Registration Number is Not provided";
			} 
			if(!$designation_id){
				$err[] = "PF Registration Number is Not provided";
			}
			if(!$email_id){
				$err[] = "email_id-ID is Not provided";
			} 
			if(!$marital_status_id){
				$err[] = "Marital status is Not provided";
			} 
			if(!$nationality){
				$err[] = "Nationality is Not provided";
			} 
			if(!$date_of_epf){
				$err[] = "Date of Epf is Not provided";
			} 
			if(!$date_of_pension){
				$err[] = "Date of Pension is Not provided";
			} 
			if(!$is_handicap){
				$err[] = "Handicap is Not provided";
			}  
			if(!$handicap_type){
				$err[] = "Handicap Type is Not provided";
			}
			if(!$next_of_kin){
				$err[] = "next of kin Type is Not provided";
			} 
			
			if(empty($err)){
				$dbOject = array(
 								'employee_code' => $employee_code,
								'gender_id' => $gender,
								'name' => $name,
								'relation' => $relation,
								'relation_name' => $relation_name,
								'phone_number' => $phone_number,
								'mobile_number' => $mobile_number,
								'adhar_number' => $aadhar_number,
								'pan_card_number' => $pan_card_number,
								'birth_date' => $birth_date,
								'joining_date' => $joining_date,
								'releaving_date' => $releaving_date,
								'address' => $address,
								'country_id' => $country_id,
								'state_id' => $state_id,
								'city_id' => $city_id,
								'bank_id' => $bank_id,
								'bank_branch_id' => $bank_branch_id,
								'account_number' => $account_number,
								'ifsc_code' => $ifsc_code,
								'esi_reg_number' => $esi_reg_number,
								'pf_reg_number' => $pf_reg_number,
								'uan' => $uan,
								'qualification_id' => $qualification_id,
								'employee_type' => $employee_type,
								'designation_id' => $designation_id,
								'email_id' => $email_id,
								'marital_status_id' => $marital_status_id,
								'nationality' => $nationality,
								'date_of_epf' => $date_of_epf,
								'date_of_pension' => $date_of_pension,
								'is_handicap' => $is_handicap,
								'handicap_type' => $handicap_type,
								'next_of_kin' => $next_of_kin, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1
								);
				
				if($this->EMPLOYEE->update($id,$dbOject)){
					redirect(base_url('employee'));
				}else{
					echo 'failed';
				}
			}else{
				print_r($err);
			}
		}else{
			print_r($err);
		}
	}


	public function delete()
	{

		$err =array();
		if($this->input->post('doDelete')){
 
			$employee_id = $this->input->post('delete_id');

			if(!$employee_id){
				$err[] = "Employee id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->EMPLOYEE->update($employee_id, $dbOject)){
					redirect(base_url('employee'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}
 
}
