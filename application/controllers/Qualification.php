<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qualification extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Qualification_model', 'QUALIFICATION', TRUE); 
    }
	
	public function index()
	{ 

	  $qualifications = $this->QUALIFICATION->getList();	
	  $data['qualifications'] = $qualifications;
	  $data['page_title']= "Qualification:Master";
	  $data['page']= "qualification/index";
      $this->load->view('components/container', $data);
	}



	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){

			$qualification = $this->input->post('qualification');
			$qualification_code = $this->input->post('qualification_code');

			if(!$qualification){
				$err[] = "Qualification is Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'qualification' => $qualification,
								'qualification_code' => $qualification_code,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1

								);
				
				if($this->QUALIFICATION->insert($dbOject)){
					redirect(base_url('qualification'));
				}else{
					echo 'failed';
				}
			}

		}
	}


	public function getQualification()
	{
		$qualification_id = $this->input->post('id');
		if(!$qualification_id){
			$err[] = "Qualification id not provided";
		}

		if(empty($err)){ 
			$qualification = $this->QUALIFICATION->getRecord($qualification_id);
			if($qualification){
				 echo json_encode(array('status'=>1,'data'=>$qualification));
			}else{
				echo 'failed';
				 
			}

		}
	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){

			$qualification_name = $this->input->post('edit_qualification_name');
			$qualification_code = $this->input->post('edit_qualification_code');
			$qualification_id = $this->input->post('edit_id');
			

			if(!$qualification_id){
				$err[] = "Qualification id not provied";
			}
			if(!$qualification_code){
				$err[] = "Qualification code not provied";
			}
			if(!$qualification_name){
				$err[] = "Qualification Not provided";
			}


			if(empty($err)){
				$dbOject = array(

								'qualification' => $qualification_name, 
								'qualification_code' => $qualification_code, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->QUALIFICATION->update($qualification_id,$dbOject)){
					redirect(base_url('qualification'));
				}else{
					echo 'failed';
				}
			}
		}
	}


	public function delete()
	{

		$err =array();
		if($this->input->post('doDelete')){
 
			$qualification_id = $this->input->post('delete_id');

			if(!$qualification_id){
				$err[] = "Qualification id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->QUALIFICATION->update($qualification_id, $dbOject)){
					redirect(base_url('qualification'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}


}
