<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salary_heads extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('Salary_head_model', 'SALARYHEAD', TRUE); 
    }
	
	public function index()
	{ 

	  $salary_heads = $this->SALARYHEAD->getList();	

	  $data['salary_heads'] = $salary_heads;
	  $data['page_title']= "Salary Heads:Master";
	  $data['page']= "salary_heads/index";
      $this->load->view('components/container', $data);
	}



	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){
	
			//print_r($this->input->post());die;

			$salary_head = $this->input->post('salary_head');
			$salary_pay_type = $this->input->post('salary_pay_type');

			if(!$salary_head){
				$err[] = "salary heads Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'salary_head' => $salary_head,
								'salary_pay_type' => $salary_pay_type,
								'created_at' => date("Y-m-d H:i:s"),
								'updated_at' => date("Y-m-d H:i:s"),
								'created_by' => 1,
								'updated_by' => 1

								);
				
				if($this->SALARYHEAD->insert($dbOject)){
					redirect(base_url('salary_heads'));
				}else{
					echo 'failed';
				}
			}

		}
	}


	public function getSalaryHeads()
	{
		$designation_id = $this->input->post('id');
		if(!$designation_id){
			$err[] = "Designation id not provied";
		}

		if(empty($err)){ 
			$designation = $this->SALARYHEAD->getRecord($designation_id);
			if($designation){
				 echo json_encode(array('status'=>1,'data'=>$designation));
			}else{
				echo 'failed';
			}
		}


	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){
			//print_r($this->input->post());die;
			$edit_salary_head = $this->input->post('edit_salary_head');
			$edit_salary_pay_type = $this->input->post('edit_salary_pay_type');
			$salary_head_id = $this->input->post('edit_id');

			if(!$edit_salary_head){
				$err[] = "Salary Head id not provied";
			}
			if(!$edit_salary_pay_type){
				$err[] = "Salary Head Not provided";
			}


			if(empty($err)){
				$dbOject = array(

								'salary_head' => $edit_salary_head, 
								'salary_pay_type' => $edit_salary_pay_type, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->SALARYHEAD->update($salary_head_id,$dbOject)){
					redirect(base_url('salary_heads'));
				}else{
					echo 'failed';
				}
			}
		}
	}


	public function delete()
	{

		$err =array();
		if($this->input->post('doDelete')){
 
			$designation_id = $this->input->post('delete_id');

			if(!$designation_id){
				$err[] = "Designation id not provied";
			} 

			if(empty($err)){
				$dbOject = array(

								'isActive' => 0, 
								'updated_at' => date("Y-m-d H:i:s"), 
								'updated_by' => 1

								);
				
				if($this->SALARYHEAD->update($designation_id, $dbOject)){
					redirect(base_url('salary_heads'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}


}
