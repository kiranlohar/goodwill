<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class State extends CI_Controller {
	 public function __construct() {
		parent::__construct (); 
		//Models
       	$this->load->model ('State_model', 'STATE', TRUE); 
   		$this->load->model ('Country_model', 'COUNTRY', TRUE); 
    }
	
	public function index()
	{ 
	  $countries = $this->COUNTRY->getList();	
	  $states = $this->STATE->getList();
	  $data['countries'] = $countries;
	  $data['states'] = $states;
	  $data['page_title']= "State:Master";
	  $data['page']= "state/index";
      $this->load->view('components/container', $data);
	}



	public function add()
	{	
		$err =array();
		if($this->input->post('doSubmit')){

			$country_id = $this->input->post('country_id');
			$state_name = $this->input->post('state_name'); 

			if(!$country_id){
				$err[] = "country id Not provided";
			}
			if(!$state_name){
				$err[] = "state name Not provided";
			}

			if(empty($err)){
				$dbOject = array(

								'country_id' => $country_id,
								'name' => $state_name,  
								'isActive' => 1,  

								);
				
				if($this->STATE->insert($dbOject)){
					redirect(base_url('state'));
				}else{
					echo 'failed';
				}
			}

		}
	}


	public function getState()
	{
		$state_id = $this->input->post('id');
		if(!$state_id){
			$err[] = "State id not provided";
		}

		if(empty($err)){ 
			$state = $this->STATE->getRecord($state_id);
			if($state){
				 echo json_encode(array('status'=>1,'data'=>$state));
			}else{
				echo 'failed';
			}
		}


	}

	public function edit()
	{
		$err =array();
		if($this->input->post('doEdit')){

			$state_id = $this->input->post('edit_id');
			$country_id = $this->input->post('edit_country_id');
			$state_name = $this->input->post('edit_state_name'); 

			if(!$state_id){
				$err[] = "state id Not provided";
			}
			if(!$country_id){
				$err[] = "country id Not provided";
			}
			if(!$state_name){
				$err[] = "state name Not provided";
			}
			if(empty($err)){
				$dbOject = array(

								'name' => $state_name, 
								'country_id' => $country_id,   
								'isActive' => 1

								);
				
				if($this->STATE->update($state_id,$dbOject)){
					redirect(base_url('state'));
				}else{
					echo 'failed';
				}
			}
		}
	}


	public function delete()
	{
		$err =array();
		if($this->input->post('doDelete')){ 
			$state_id = $this->input->post('delete_id');
			if(!$state_id){
				$err[] = "State id not provied";
			} 
			if(empty($err)){
				$dbOject = array(
								'isActive' => 0   
								);
				if($this->STATE->update($state_id, $dbOject)){
					redirect(base_url('state'));
				}else{
					echo 'failed';
				}
			}
			// echo 'yeees';
		}
			// echo "okk";die;
	}

public function getAjaxState()
{
	$country_id = $this->input->post('country');
	$stateData = $this->STATE->getStateByCountry($country_id);
	echo json_encode($stateData);
}

}
