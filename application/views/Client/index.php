<?php ?>
     <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-4">
							<h4 class="page-title">Client Details</h4>
						</div>
						<div class="col-xs-8 text-right m-b-30">
							<a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#add_client"><i class="fa fa-plus"></i>Add Client Details</a>
						</div>
					</div>
					<!-- <div class="row filter-row">
						<div class="col-sm-3 col-xs-6">  
							<div class="form-group form-focus">
								<label class="control-label">Name</label>
								<input type="text" class="form-control floating" />
							</div>
						</div>
						<div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Company</label>
								<select class="select floating"> 
									<option value="">Select Company</option>
									<option>Global Technologies</option>
									<option>Delta Infotech</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Role</label>
								<select class="select floating"> 
									<option value="">Select Roll</option>
									<option value="">Web Developer</option>
									<option value="1">Web Designer</option>
									<option value="1">Android Developer</option>
									<option value="1">Ios Developer</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6">  
							<a href="#" class="btn btn-success btn-block"> Search </a>  
						</div>     
                    </div> -->
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table datatable">
									<thead>
										<tr>
											<th>#Sr. No</th>
											<th style="">Client Type</th> 
											<th style="">Name</th> 
											<th style="">Mobile Number</th> 
											<th style="width: 20%">Address</th> 
											<th>Adhar Number</th> 
											<th>PAN Number</th> 
											<th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									$i = 1;
									foreach ($clients as $client) { ?>
									
									<tr> 
										<td><?php echo $i; ?></td>
										<td>
											<?php
												if($client['client_type'] == 1)
												{
													echo "Individual";
												}else{
													echo "Firm"; 	
												}
											?>
												
										</td>
										<td><?php echo $client['name'];?></td>
										<td><?php echo $client['mobile_number'];?></td>
										<td><?php echo $client['address'];?></td>
										<td><?php echo $client['adhar_number'];?></td>
										<td><?php echo $client['pan_card_number'];?></td>
										<td class="text-right">
											<div class="dropdown">
												<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
												<ul class="dropdown-menu pull-right">
													<li><a href="#" onclick="showEdit(<?php echo $client['id']; ?>)"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
													<li><a href="#" onclick="showDelete(<?php echo $client['id']; ?>)"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
												</ul>
											</div>
										</td>
									</tr>
									<?php 
										$i++;
										} ?>										  
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
    </div>
    <div id="add_client" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Add Client</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('client/add');?>" method="post">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group form-check form-check-inlin">
	                               <label class="block">Employee Type</label>
	                               <label class="radio-inline">
	                                   <input name="client_type" id="individual" checked="checked" type="radio" value="1"> Individual
	                               </label>
	                               <label class="radio-inline">
	                                   <input name="client_type" id="firm" type="radio" value="2"> Firm
	                               </label>
                           		</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="name" id="name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="phone_number" id="phone_number">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Mobile Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="mobile_number" id="mobile_number">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Aadhar Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="adhar_number" id="adhar_number">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">PAN Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="pan_card_number" id="pan_card_number">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Birth Date<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="birth_date" id="birth_date">
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Address<span class="text-danger">*</span></label>
									<textarea class="form-control" rows="10" name="address" id="address"></textarea>
								</div>
							</div>						
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Country<span class="text-danger">*</span></label>
									<select class="select floating" name="country_id" id="country_id"> 
									<option value="">Select Country</option>
									<?php 
									foreach ($countries as $country) { ?>
										<option value="<?php echo $country['id'];?>"><?php echo $country['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">State<span class="text-danger">*</span></label>
									<select class="select floating" name="state_id" id="state_id"> 
									<option value="">Select State</option> 
									</select> 
								</div>
							</div>
							<input type="hidden" name="" id="city_url" value="<?php echo base_url('city/getAjaxCity'); ?>">		
							<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">


							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">City<span class="text-danger">*</span></label>
									<select class="select floating" name="city_id" id="city_id"> 
									<option value="">Select City</option> 
									</select> 
								</div>
							</div> 
						</div> 
						<div class="m-t-20 text-center">
							<button type="submit" name="doSubmit" value="doSubmit" class="btn btn-primary">Add Clients</button>
							<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="edit_client" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Edit Client</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('client/edit');?>" method="post">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group">
	                               <label class="block">Employee Type</label>
	                               <label class="radio-inline">
	                                   <input name="edit_client_type" id="edit_individual" checked="checked" type="radio" value="1"> Individual
	                               </label>
	                               <label class="radio-inline">
	                                   <input name="edit_client_type" id="edit_firm" type="radio" value="2"> Firm
	                               </label>
                           		</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_name" id="edit_name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_phone_number" id="edit_phone_number">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Mobile Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_mobile_number" id="edit_mobile_number">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Aadhar Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_adhar_number" id="edit_adhar_number">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">PAN Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_pan_card_number" id="edit_pan_card_number">
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Birth Date<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="edit_birth_date" id="edit_birth_date">
								</div>
							</div>	

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Address<span class="text-danger">*</span></label>
									<textarea class="form-control" rows="10" name="edit_address" id="edit_address"></textarea>
								</div>
							</div>						
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Country<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_country_id" id="edit_country_id"> 
									<option value="">Select Country</option>
									<?php 
									foreach ($countries as $country) { ?>
										<option value="<?php echo $country['id'];?>"><?php echo $country['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">State<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_state_id" id="edit_state_id"> 
									<option value="">Select States</option>
									<?php 
									foreach ($states as $state) { ?>
										<option value="<?php echo $state['id'];?>"><?php echo $state['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">City<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_city_id" id="edit_city_id"> 
									<option value="">Select City</option>
									<?php 
									foreach ($cities as $city) { ?>
										<option value="<?php echo $city['id'];?>"><?php echo $city['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div> 
						</div> 
						<input class="form-control" type="hidden" name="edit_id" id="edit_id" value="">
						<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">
						<div class="m-t-20 text-center">
							<button type="submit" name="doEdit" value="doEdit" class="btn btn-primary">Update Client</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="delete_client" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modal-md">
				<div class="modal-header">
					<h4 class="modal-title">Delete Client</h4>
				</div>
				<form action="<?php echo base_url('client/delete');?>" method="post">
					<div class="modal-body card-box">
						<p>Are you sure want to delete this?</p>
						<input type="hidden" name="delete_id" id="delete_id" value="">
					<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<button type="submit" name="doDelete" value="doDelete"  class="btn btn-danger">Delete</button>
					</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 


<?php ?>

<script type="text/javascript">	
function showEdit(id){
 	var bs_url='<?php echo base_url('client/getClient') ?>';
 	
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) { 
		console.log(respo);
		$('#edit_name').val(respo.data.name);
		
		// $("#edit_firm").val(respo.data.client_type).trigger('change');

		if(respo.data.client_type == 1)
		{
			$("#edit_individual").attr('checked','checked');
			$("#edit_firm").removeAttr('checked');
			$( "#edit_phone_number" ).attr('disabled', false);
			$('#client_type').val(1);
		} else if(respo.data.client_type == 2)
		{
			$("#edit_firm").attr('checked','checked');
			$("#edit_individual").removeAttr('checked');
			$( "#edit_phone_number" ).attr('disabled', true);
			$('#client_type').val(2);
		}


		$('#edit_phone_number').val(respo.data.phone_number);
		$('#edit_mobile_number').val(respo.data.mobile_number);
		$('#edit_adhar_number').val(respo.data.adhar_number); 
		$('#edit_pan_card_number').val(respo.data.pan_card_number); 
		$('#edit_birth_date').val(respo.data.birth_date); 
		$('#edit_address').val(respo.data.address); 
		$('#edit_id').val(respo.data.id);
		$("#edit_country_id").val(respo.data.country_id).trigger('change');
		$("#edit_state_id").val(respo.data.state_id).trigger('change');
		$("#edit_city_id").val(respo.data.city_id).trigger('change');

		$("#edit_client").modal('show');
	});
	 
}	


function showDelete(id){
 	var bs_url='<?php echo base_url('client/getClient') ?>';
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) {  
		$('#delete_id').val(respo.data.id);
		$("#delete_client").modal('show');
	});
	 
}	

</script>
