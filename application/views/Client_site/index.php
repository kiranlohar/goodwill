<?php ?>
     <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-4">
							<h4 class="page-title">Client site Details</h4>
						</div>
						<div class="col-xs-8 text-right m-b-30">
							<a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#add_client_site"><i class="fa fa-plus"></i>Add Client site Details</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table datatable">
									<thead>
										<tr>
											<th>#Sr. No</th>
											<th style="">Client</th> 
											<th style="">Site Name</th> 
											<th style="">Address</th> 
											<th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
									<?php 
										$i = 1;
										foreach ($client_sites as $clientsite) { ?>
									
									<tr> 
										<td><?php echo $i; ?></td>
										<td><?php echo $clientsite['client_name'];?></td>
										<td><?php echo $clientsite['site_name'];?></td>
										<td><?php echo $clientsite['site_address'];?></td>
										<td class="text-right">
											<div class="dropdown">
												<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
												<ul class="dropdown-menu pull-right">
													<li><a href="#" onclick="showEdit(<?php echo $clientsite['id']; ?>)"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
													<li><a href="#" onclick="showDelete(<?php echo $clientsite['id']; ?>)"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
												</ul>
											</div>
										</td>
									</tr>
									<?php
									$i++;
									 } ?>										  
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
    </div>
    <div id="add_client_site" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Add Client Site</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('client_site/add');?>" method="post">
						<div class="row">	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Client ID<span class="text-danger">*</span></label>
									<select class="select floating" name="client_id" id="client_id"> 
									<option value="">Select Client</option>
									<?php 
									foreach ($clients as $client) { ?>
										<option value="<?php echo $client['id'];?>"><?php echo $client['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Site Name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="site_name" id="site_name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Site Address<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="site_address" id="site_address">
								</div>
							</div>					
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Country<span class="text-danger">*</span></label>
									<select class="select floating" name="country_id" id="country_id" role="presentation"> 
									<option value="">Select Country</option>
									<?php 
									foreach ($countries as $country) { ?>
										<option value="<?php echo $country['id'];?>"><?php echo $country['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">State<span class="text-danger">*</span></label>
									<select class="select floating" name="state_id" id="state_id"> 
										<option value="">Select State</option> 
									</select>
								</div>
							</div>
							<input type="hidden" name="" id="city_url" value="<?php echo base_url('city/getAjaxCity'); ?>">		
							<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">City<span class="text-danger">*</span></label>
									<select class="select floating" name="city_id" id="city_id"> 
										<option value="">Select City</option> 
									</select> 
								</div>
							</div> 
						</div> 
						<div class="m-t-20 text-center">
							<button type="submit" name="doSubmit" value="doSubmit" class="btn btn-primary">Add Client Site</button>
							<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="edit_client_site" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Edit Client Site</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('client_site/edit');?>" method="post">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Client ID<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_client_id" id="edit_client_id"> 
									<option value="">Select Client</option>
									<?php 
									foreach ($clients as $client) { ?>
										<option value="<?php echo $client['id'];?>"><?php echo $client['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Site Name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_site_name" id="edit_site_name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Site Address<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_site_address" id="edit_site_address">
								</div>
							</div>					
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Country<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_country_id" id="edit_country_id"> 
									<option value="">Select Country</option>
									<?php 
									foreach ($countries as $country) { ?>
										<option value="<?php echo $country['id'];?>"><?php echo $country['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">State<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_state_id" id="edit_state_id"> 
									<option value="">Select Country</option>
									<?php 
									foreach ($states as $state) { ?>
										<option value="<?php echo $state['id'];?>"><?php echo $state['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">City<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_city_id" id="edit_city_id"> 
									<option value="">Select City</option>
									<?php 
									foreach ($cities as $city) { ?>
										<option value="<?php echo $city['id'];?>"><?php echo $city['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div> 
						</div> 
						<input class="form-control" type="hidden" name="edit_id" id="edit_id" value="">
						<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">
						<div class="m-t-20 text-center">
							<button type="submit" name="doEdit" value="doEdit" class="btn btn-primary">Update Client Site</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="delete_client_site" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modal-md">
				<div class="modal-header">
					<h4 class="modal-title">Delete Client Site</h4>
				</div>
				<form action="<?php echo base_url('client_site/delete');?>" method="post">
					<div class="modal-body card-box">
						<p>Are you sure want to delete this?</p>
						<input type="hidden" name="delete_id" id="delete_id" value="">
					<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<button type="submit" name="doDelete" value="doDelete"  class="btn btn-danger">Delete</button>
					</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 


<?php ?>

<script type="text/javascript">	
function showEdit(id){
 	var bs_url='<?php echo base_url('client_site/getClient_site') ?>';
 	
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) { 
		console.log(respo);
		$('#edit_name').val(respo.data.name);
		
		// $("#edit_firm").val(respo.data.client_type).trigger('change');

		$('#edit_address').val(respo.data.address); 
		$('#edit_id').val(respo.data.id);
		$("#edit_client_id").val(respo.data.client_id).trigger('change');
		$("#edit_country_id").val(respo.data.country_id).trigger('change');
		$("#edit_state_id").val(respo.data.state_id).trigger('change');
		$("#edit_city_id").val(respo.data.city_id).trigger('change');
		$('#edit_site_name').val(respo.data.site_name);
		$('#edit_site_address').val(respo.data.site_address);
		$("#edit_client_site").modal('show');
	});
	 
}	


function showDelete(id){
 	var bs_url='<?php echo base_url('client_site/getClient_site') ?>';
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) {  
		$('#delete_id').val(respo.data.id);
		$("#delete_client_site").modal('show');
	});
	 
}	

</script>
