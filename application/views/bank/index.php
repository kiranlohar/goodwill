<?php ?>
     <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-4">
							<h4 class="page-title">Bank</h4>
						</div>
						<div class="col-xs-8 text-right m-b-30">
							<a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#add_bank"><i class="fa fa-plus"></i> Add Bank</a>
						</div>
					</div>
					<!-- <div class="row filter-row">
						<div class="col-sm-3 col-xs-6">  
							<div class="form-group form-focus">
								<label class="control-label">Name</label>
								<input type="text" class="form-control floating" />
							</div>
						</div>
						<div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Company</label>
								<select class="select floating"> 
									<option value="">Select Company</option>
									<option>Global Technologies</option>
									<option>Delta Infotech</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Role</label>
								<select class="select floating"> 
									<option value="">Select Roll</option>
									<option value="">Web Developer</option>
									<option value="1">Web Designer</option>
									<option value="1">Android Developer</option>
									<option value="1">Ios Developer</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6">  
							<a href="#" class="btn btn-success btn-block"> Search </a>  
						</div>     
                    </div> -->
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table datatable">
									<thead>
										<tr>
											<th>#Bank ID</th>
											<th style="width:30%;">Bank</th> 
											<!-- <th>Created By</th>  -->
											<th>Created Date</th> 
											<th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($banks as $bank) { ?>
										
										<tr> 
											<td><?php echo $bank['id'];?></td>
											<td><?php echo $bank['bank_name'];?></td>
											<!-- <td>
												<span class="label label-danger-border">Admin</span>
											</td> -->
											<td><?php echo $bank['created_at'];?></td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" onclick="showEdit(<?php echo $bank['id']; ?>)"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#" onclick="showDelete(<?php echo $bank['id']; ?>)"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
									<?php } ?>										 
										 
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
    </div>
    <div id="add_bank" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Add Bank</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('bank/add');?>" method="post">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label">Bank<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="bank_name">
								</div>
							</div>  
						</div> 
						<div class="m-t-20 text-center">
							<button type="submit" name="doSubmit" value="doSubmit" class="btn btn-primary">Add Bank</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="edit_bank" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Edit Bank</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('bank/edit');?>" method="post">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label">Bank<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_bank_name" id="edit_bank_name" value="">
									<input class="form-control" type="hidden" name="edit_id" id="edit_id" value="">
								</div>
							</div>  
						</div> 
						<div class="m-t-20 text-center">
							<button type="submit" name="doEdit" value="doEdit" class="btn btn-primary">Update Bank</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="delete_bank" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modal-md">
				<div class="modal-header">
					<h4 class="modal-title">Delete Bank</h4>
				</div>
				<form action="<?php echo base_url('bank/delete');?>" method="post">
					<div class="modal-body card-box">
						<p>Are you sure want to delete this?</p>
						<input type="hidden" name="delete_id" id="delete_id" value="">
					<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<button type="submit" name="doDelete" value="doDelete"  class="btn btn-danger">Delete</button>
					</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 


<?php ?>



<script type="text/javascript">
	
function showEdit(id){
 	var bs_url='<?php echo base_url('bank/getBank') ?>';
 	
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) { 
		$('#edit_bank_name').val(respo.data.bank_name);
		$('#edit_id').val(respo.data.id);
		$("#edit_bank").modal('show');
	});
	 
}	


function showDelete(id){
 	console.log(id);
 	var bs_url='<?php echo base_url('bank/getBank') ?>';
 	var bs_url='<?php echo base_url('bank/getBank') ?>';
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) {  
		console.log(respo);
		$('#delete_id').val(respo.data.id);
		$("#delete_bank").modal('show');
	});
	 
}		

</script>