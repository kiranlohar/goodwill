<?php ?>
     <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-4">
							<h4 class="page-title">City</h4>
						</div>
						<div class="col-xs-8 text-right m-b-30">
							<a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#add_city"><i class="fa fa-plus"></i> Add City</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table datatable">
									<thead>
										<tr>
											<th>#Cit ID</th>
											<th style="">State</th>   
											<th style="">City</th>   
											<th class="text-right">Action</th>
									</thead>
									<tbody>
										<?php foreach ($cities as $city) { ?>
										
										<tr> 
											<td><?php echo $city['id'];?></td>
											<td><?php echo $city['state_name'];?></td>
											<td><?php echo $city['name'];?></td>   
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" onclick="showEdit(<?php echo $city['id']; ?>)"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#" onclick="showDelete(<?php echo $city['id']; ?>)"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
									<?php } ?>										 
										 
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
    </div>
    <div id="add_city" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Add Cities</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('city/add');?>" method="post">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Country<span class="text-danger">*</span></label>
									<select class="select floating" name="country_id" id="country_id"> 
									<option value="">Select Country</option>
									<?php 
									foreach ($countries as $country) { ?>
										<option value="<?php echo $country['id'];?>"><?php echo $country['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>  

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">States<span class="text-danger">*</span></label>
									<select class="select floating" name="state_id" id="state_id" role="presentation"> 
									<option value="">Select States</option>
									<?php 
									foreach ($states as $state) { ?>
										<option value="<?php echo $state['id'];?>"><?php echo $state['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">City Name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="name">
								</div>
							</div>   
						</div> 
						<div class="m-t-20 text-center">
							<button type="submit" name="doSubmit" value="doSubmit" class="btn btn-primary">Add City</button>
							<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="edit_city" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Edit City</h4>
				</div>
				<div class="modal-body">
				<form class="m-b-30" action="<?php echo base_url('city/edit');?>" method="post">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Country<span class="text-danger">*</span></label>
							<select class="select floating" name="edit_country_id" id="edit_country_id" disabled="true"> 
							<option value="">Select Country</option>
							<?php 
							foreach ($countries as $country) { ?>
								<option value="<?php echo $country['id'];?>"><?php echo $country['name']; ?></option>
							<?php } ?>
							</select> 
						</div>
					</div>  

							<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">State<span class="text-danger">*</span></label>
								<select class="select floating" name="edit_state_id" id="edit_state_id" disabled="true"> 
								<option value="">Select States</option>
								<?php 
								foreach ($states as $state) { ?>
									<option value="<?php echo $state['id'];?>"><?php echo $state['name']; ?></option>
								<?php } ?>
								</select> 
							</div>
						</div>  
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">City<span class="text-danger">*</span></label>
								<input class="form-control" id="edit_city_name" type="text" name="edit_city_name" value="">
							</div>
						</div>   
						<input class="form-control" type="hidden" name="edit_id" id="edit_id" value="">
					</div> 
					<div class="m-t-20 text-center">
						<button type="submit" name="doEdit" value="doEdit" class="btn btn-primary">Update City</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
	<div id="delete_city" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modal-md">
				<div class="modal-header">
					<h4 class="modal-title">Delete City</h4>
				</div>
				<form action="<?php echo base_url('city/delete');?>" method="post">
					<div class="modal-body card-box">
						<p>Are you sure want to delete this?</p>
						<input type="hidden" name="delete_id" id="delete_id" value="">
					<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<button type="submit" name="doDelete" value="doDelete"  class="btn btn-danger">Delete</button>
					</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 


<?php ?>

<script type="text/javascript">
	
function showEdit(id){
 	var bs_url='<?php echo base_url('city/getcity') ?>';
 	
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) { 
	    console.log(respo);
		$('#edit_city_name').val(respo.data.name);  
		$('#edit_id').val(respo.data.id);
		//$("#edit_state_id").val(respo.data.edit_state_id);
		$("#edit_city").modal('show');
	});
	 
}	


function showDelete(id){
 	var bs_url='<?php echo base_url('city/getCity') ?>'; 
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) {  
		$('#delete_id').val(respo.data.id);
		$("#delete_city").modal('show');
	});
	 
}
</script>