<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('components/header');
    $this->load->view('components/sidebar');
    $this->load->view($page);
    $this->load->view('components/footer');
?>


