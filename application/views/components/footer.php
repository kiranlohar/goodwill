<div class="sidebar-overlay" data-reff="#sidebar"></div>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js'); ?>"></script>       
		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slimscroll.js'); ?>"></script>		
		<script type="text/javascript" src="<?php echo base_url('assets/plugins/raphael/raphael-min.js'); ?>"></script>  
		<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.dataTables.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js');?>"></script> 
		<script type="text/javascript" src="<?php echo base_url('assets/js/select2.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/moment.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/app.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/validate.js');?>"></script>


<script type="text/javascript">
  $("#country_id,#edit_country_id").change(function() {
   var e = $(this).val(),
       t = $("#state_url").val();
   $("#state_id,#edit_state_id").empty(), $.ajax({
       type: "POST",
       url: t,
       data: {
           country: e
       },
       success: function(e) {
           var t = JSON.parse(e);
           // console.log(t);

           $("#state_id,#edit_state_id").append('<option value="" disabled selected>Select State</option>'), $.each(t, function(e, t) {
               $("#state_id,#edit_state_id").append($("<option></option>").attr("value", t.id).text(t.name))
           })
       }
   })
});


$("#state_id,#edit_state_id").change(function() {
   var e = $(this).val(),
       t = $("#city_url").val();
   $("#city_id,#edit_city_id").empty(), $.ajax({
       type: "POST",
       url: t,
       data: {
           state_id: e
       },
       success: function(e) {
           var t = JSON.parse(e); 

           $("#city_id,#edit_city_id").append('<option value="" disabled selected>Select City</option>'), $.each(t, function(e, t) {
               $("#city_id,#edit_city_id").append($("<option></option>").attr("value", t.id).text(t.name))
           })
       }
   })
});

 

$("#bank_id,#edit_bank_id").change(function() {
   var e = $(this).val(),
       t = $("#bank_branch_url").val();
   $("#bank_branch_id,#edit_bank_branch_id").empty(), $.ajax({
       type: "POST",
       url: t,
       data: {
           bank_id: e
       },
       success: function(e) {
           var t = JSON.parse(e); 

           $("#bank_branch_id,#edit_bank_branch_id").append('<option value="" disabled selected>Select Bank Branch</option>'), $.each(t, function(e, t) {
               $("#bank_branch_id,#edit_bank_branch_id").append($("<option></option>").attr("value", t.id).text(t.branch_name))
           })
       }
   })
});


$(document).ready(function() {
   $('#is_handicap_yes,#edit_is_handicap_yes').click(function() {
        $('#handicap_type_div,#edit_handicap_type_div').show();
   });
   $('#is_handicap_no,#edit_is_handicap_no').click(function() {
        $('#handicap_type_div,#edit_handicap_type_div').hide();
        
   });


});


</script>

<script type="text/javascript">
 $(document).ready(function() {

   $( "#individual" ).click(function() {
  $( "#edit_phone_number" ).attr('disabled', false);
  $( "#phone_number" ).attr('disabled', false);
 }); 

 $( "#firm" ).click(function() {
    $( "#phone_number" ).attr('disabled', true);
    $( "#edit_phone_number" ).attr('disabled', true);
 }); 

   $( "#edit_firm" ).click(function() {
    $( "#edit_phone_number" ).attr('disabled', true);
    $( "#edit_phone_number" ).val('');
 }); 

 $( "#edit_individual" ).click(function() {
    $( "#edit_phone_number" ).attr('disabled', false);
 }); 
}); 
</script>
		
</body>
 
</html>
