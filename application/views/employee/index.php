<?php ?>
     <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-4">
							<h4 class="page-title">Employee Details</h4>
						</div>
						<div class="col-xs-8 text-right m-b-30">
							<a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#add_employee_details"><i class="fa fa-plus"></i> Add Employee</a>
						</div>
					</div>
					<!-- <div class="row filter-row">
						<div class="col-sm-3 col-xs-6">  
							<div class="form-group form-focus">
								<label class="control-label">Name</label>
								<input type="text" class="form-control floating" />
							</div>
						</div>
						<div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Company</label>
								<select class="select floating"> 
									<option value="">Select Company</option>
									<option>Global Technologies</option>
									<option>Delta Infotech</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Role</label>
								<select class="select floating"> 
									<option value="">Select Roll</option>
									<option value="">Web Developer</option>
									<option value="1">Web Designer</option>
									<option value="1">Android Developer</option>
									<option value="1">Ios Developer</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6">  
							<a href="#" class="btn btn-success btn-block"> Search </a>  
						</div>     
                    </div> -->
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table datatable">
									<thead>
										<tr>
											<th>#Employee ID</th>
											<th style="">Name</th> 
											<th style="">Mobile Number</th> 
											<th style="width: 20%">Address</th>  
											<th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($employee_details as $employee_detail) { ?>
										
										<tr> 
											<td><?php echo $employee_detail['employee_code'];?></td>
											<td><?php echo $employee_detail['name'];?></td>
											<td><?php echo $employee_detail['mobile_number'];?></td>
											<td><?php echo $employee_detail['address'];?></td>  
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" onclick="showEdit(<?php echo $employee_detail['id']; ?>)"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#" onclick="showDelete(<?php echo $employee_detail['id']; ?>)"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
									<?php } ?>										 
										 
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
    </div>
    <div id="add_employee_details" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Add Employee Details</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('employee/add');?>" method="post">
						<div class="row">  
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Employee Code<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="employee_code">
								</div>
							</div>		 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Gender<span class="text-danger">*</span></label>
									<select class="select floating" name="gender" id="gender"> 
									<option value="">Select Gender</option>
									<?php 
									foreach ($genders as $gender) { ?>
										<option value="<?php echo $gender['id'];?>"><?php echo $gender['gender']; ?></option>
									<?php } ?>
									</select> 
								</div>
                           	</div></br>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="name">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Relation<span class="text-danger">*</span></label>
									<select class="form-control" name="relation">
									 
										<option>Father</option>
										<option>Husband</option>
									</select>
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Relation name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="relation_name">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="phone_number">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Mobile Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="mobile_number">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Aadhar Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="aadhar_number">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">PAN Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="pan_card_number">
								</div>
							</div>  
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Birth Date<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="birth_date">
								</div>
							</div>  
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Joining Date<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="joining_date">
								</div>
							</div> 

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Releving Date<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="releaving_date">
								</div>
							</div> 

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Address<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="address">
								</div>
							</div>


							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Country<span class="text-danger">*</span></label>
									<select class="select floating" name="country_id" id="country_id"> 
									<option value="">Select Country</option>
									<?php 
									foreach ($countries as $country) { ?>
										<option value="<?php echo $country['id'];?>"><?php echo $country['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">State<span class="text-danger">*</span></label>
									<select class="form-control" name="state_id" id="state_id">
										<option>Maharashtra</option>
										<option>Delhi</option>
									</select>
								</div>
							</div>   
							<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">City<span class="text-danger">*</span></label>
									<select class="form-control" name="city_id" id="city_id">
										<option value="">Select City</option>
									</select>
								</div>
							</div> 
							<input type="hidden" name="" id="city_url" value="<?php echo base_url('city/getAjaxCity'); ?>">							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Bank<span class="text-danger">*</span></label>
									<select class="form-control" name="bank_id" id="bank_id">
									<option value="">Select Bank</option>
									<?php foreach ($banks as $bank) { ?>
										<option value="<?php echo $bank['id'];?>"><?php echo $bank['bank_name']; ?></option>
									<?php } ?>
									</select>
								</div>
							</div> 
							<input type="hidden" id="bank_branch_url" value="<?php echo base_url('bank_branch/getAjaxBranch'); ?>">							
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Bank Branch<span class="text-danger">*</span></label>
									<select class="form-control" name="bank_branch_id" id="bank_branch_id">
										<option value="">Select Bank Branch</option> 
									</select>
								</div>
							</div> 
							

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Account Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="account_number">
								</div>
							</div> 	 

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Bank IFSC Code<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="ifsc_code">
								</div>
							</div> 	

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">ESI Registration Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="esi_reg_number">
								</div>
							</div> 	

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">PF Registration Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="pf_reg_number">
								</div>
							</div> 

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">UAN<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="uan">
								</div>
							</div> 	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Qualification<span class="text-danger">*</span></label>
									<select class="form-control" name="qualification_id">
										<option value="">Select Qualification</option>
										<?php foreach ($qualifications as $qualification) { ?>
											<option value="<?php echo $qualification['id'];?>"><?php echo $qualification['qualification_code']; ?> - <?php echo $qualification['qualification']; ?></option>
										<?php } ?> 
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Employee Type<span class="text-danger">*</span></label>
									<select class="form-control" name="employee_type">
										<option value="">Select Employee Type</option>										
										<option value="1">Permanent</option>
										<option value="2">Temporary</option>
									</select>
								</div>
							</div>  
			
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Department <span class="text-danger">*</span></label>
									<select class="form-control" name="department_id">
										<option value="">Select Department</option>										

										<?php foreach ($departments as $department) { ?>
										<option value="<?php echo $department['id'];?>"><?php echo $department['department_name']; ?></option>
										<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Designation<span class="text-danger">*</span></label>
									<select class="form-control" name="designation_id"> 
										<option value="">Select Designation</option>										

										<?php foreach ($designations as $designation) { ?>
										<option value="<?php echo $designation['id'];?>"><?php echo $designation['designation']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>	
							<div class="col-md-6">
									<div class="form-group">
									<label class="control-label">Email-ID<span class="text-danger">*</span></label>
									<input class="form-control" type="email" name="email_id">
								</div>
							</div>  

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">marital Status <span class="text-danger">*</span></label>
									<select class="form-control" name="marital_status_id">
										<option value="">Select marital Status</option>										

										<?php foreach ($marital_status as $marital) { ?>
										<option value="<?php echo $marital['id'];?>"><?php echo $marital['marital_status']; ?></option>
										<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Nationality<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="nationality">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Date of EPF<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="date_of_epf">
								</div>
							</div> 	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Date of Pension<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="date_of_pension">
								</div>
							</div> 	
							<div class="col-md-6">
								<div class="form-group">
	                               <label class="control-label">Is Handicap</label>
	                               <label class="radio-inline">
	                                   <input name="is_handicap" id="is_handicap_yes" checked="checked" type="radio"> Yes
	                               </label>
	                               <label class="radio-inline">
	                                   <input name="is_handicap" id="is_handicap_no" type="radio"> No
	                               </label>
                           		</div> 
                           	</div>

							<div class="col-md-6" id="handicap_type_div">
								<div class="form-group">
									<label class="control-label">Handicap Type<span class="text-danger">*</span></label>
									<select class="form-control" name="handicap_type">
										<option value="L">L</option>
										<option value="H">H</option>
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Nexr Of Kin<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="next_of_kin">
								</div>
							</div>  
							<div class="m-t-20 text-center">
								<button type="submit" name="doSubmit" value="doSubmit" class="btn btn-primary">Add Employee Detail</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
    <div id="edit_employee_details" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Edit Employee Details</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('employee/edit');?>" method="post">
						<div class="row">  
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Employee Code<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_employee_code" id="edit_employee_code">
								</div>
							</div>		 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Gender<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_gender_id" id="edit_gender_id"> 
									<option value="">Select Gender</option>
									<?php 
									foreach ($genders as $gender) { ?>
										<option value="<?php echo $gender['id'];?>"><?php echo $gender['gender']; ?></option>
									<?php } ?>
									</select> 
								</div>
                           	</div></br>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_name" id="edit_name">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Relation<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_relation" id="edit_relation">
									 
										<option>Father</option>
										<option>Husband</option>
									</select>
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Relation name<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_relation_name" id="edit_relation_name">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_phone_number" id="edit_phone_number">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Mobile Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_mobile_number" id="edit_mobile_number">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Aadhar Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_aadhar_number" id="edit_aadhar_number">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">PAN Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_pan_card_number" id="edit_pan_card_number">
								</div>
							</div>  
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Birth Date<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="edit_birth_date" id="edit_birth_date">
								</div>
							</div>  
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Joining Date<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="edit_joining_date" id="edit_joining_date">
								</div>
							</div> 

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Releving Date<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="edit_releaving_date" id="edit_releaving_date">
								</div>
							</div> 

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Address<span class="text-danger">*</span></label>
									<textarea class="form-control" name="edit_address" id="edit_address" rows="9"> </textarea>
								</div>
							</div>


							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Country<span class="text-danger">*</span></label>
									<select class="select floating" name="edit_country_id" id="edit_country_id"> 
									<option value="">Select Country</option>
									<?php 
									foreach ($countries as $country) { ?>
										<option value="<?php echo $country['id'];?>"><?php echo $country['name']; ?></option>
									<?php } ?>
									</select> 
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">State<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_state_id" id="edit_state_id">
										<option value="">Select State</option>										 
									</select>
								</div>
							</div>   
							<input type="hidden" name="" id="state_url" value="<?php echo base_url('state/getAjaxState'); ?>">
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">City<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_city_id" id="edit_city_id">
										<option value="">Select City</option>
									</select>
								</div>
							</div> 
							<input type="hidden" name="" id="city_url" value="<?php echo base_url('city/getAjaxCity'); ?>">							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Bank<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_bank_id" id="edit_bank_id">
									<option value="">Select Bank</option>
									<?php foreach ($banks as $bank) { ?>
										<option value="<?php echo $bank['id'];?>"><?php echo $bank['bank_name']; ?></option>
									<?php } ?>
									</select>
								</div>
							</div> 
							<input type="hidden" id="bank_branch_url" value="<?php echo base_url('bank_branch/getAjaxBranch'); ?>">							
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Bank Branch<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_bank_branch_id" id="edit_bank_branch_id">
										<option value="">Select Bank Branch</option> 
									</select>
								</div>
							</div> 
							

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Account Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_account_number" id="edit_account_number">
								</div>
							</div> 	 

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Bank IFSC Code<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_ifsc_code" id="edit_ifsc_code">
								</div>
							</div> 	

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">ESI Registration Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_esi_reg_number" id="edit_esi_reg_number">
								</div>
							</div> 	

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">PF Registration Number<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_pf_reg_number" id="edit_pf_reg_number">
								</div>
							</div> 

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">UAN<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_uan" id="edit_uan">
								</div>
							</div> 	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Qualification<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_qualification_id" id="edit_qualification_id">
										<option value="">Select Qualification</option>
										<?php foreach ($qualifications as $qualification) { ?>
											<option value="<?php echo $qualification['id'];?>"><?php echo $qualification['qualification_code']; ?> - <?php echo $qualification['qualification']; ?></option>
										<?php } ?> 
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Employee Type<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_employee_type" id="edit_employee_type">
										<option value="">Select Employee Type</option>										
										<option value="1">Permanent</option>
										<option value="2">Temporary</option>
									</select>
								</div>
							</div>  
			
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Department <span class="text-danger">*</span></label>
									<select class="form-control" name="edit_department_id" id="edit_department_id">
										<option value="">Select Department</option>
										<?php foreach ($departments as $department) { ?>
										<option value="<?php echo $department['id'];?>"><?php echo $department['department_name']; ?></option>
										<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Designation<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_designation_id" id="edit_designation_id"> 
										<option value="">Select Designation</option>										

										<?php foreach ($designations as $designation) { ?>
										<option value="<?php echo $designation['id'];?>"><?php echo $designation['designation']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>	
							<div class="col-md-6">
									<div class="form-group">
									<label class="control-label">Email-ID<span class="text-danger">*</span></label>
									<input class="form-control" type="email" name="edit_email_id" id="edit_email_id">
								</div>
							</div>  

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">marital Status <span class="text-danger">*</span></label>
									<select class="form-control" name="edit_marital_status_id" id="edit_marital_status_id">
										<option value="">Select marital Status</option>										

										<?php foreach ($marital_status as $marital) { ?>
										<option value="<?php echo $marital['id'];?>"><?php echo $marital['marital_status']; ?></option>
										<?php } ?>
									</select> 
								</div>
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Nationality<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_nationality" id="edit_nationality">
								</div>
							</div> 
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Date of EPF<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="edit_date_of_epf" id="edit_date_of_epf">
								</div>
							</div> 	
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Date of Pension<span class="text-danger">*</span></label>
									<input class="form-control" type="date" name="edit_date_of_pension" id="edit_date_of_pension">
								</div>
							</div> 	
							<div class="col-md-6">
								<div class="form-group">
	                               <label class="control-label">Is Handicap</label>
	                               <label class="radio-inline">
	                                   <input name="edit_is_handicap" id="edit_is_handicap_yes" checked="checked" type="radio"> Yes
	                               </label>
	                               <label class="radio-inline">
	                                   <input name="edit_is_handicap" id="edit_is_handicap_no" type="radio"> No
	                               </label>
                           		</div> 
                           	</div>

							<div class="col-md-6" id="edit_handicap_type_div">
								<div class="form-group">
									<label class="control-label">Handicap Type<span class="text-danger">*</span></label>
									<select class="form-control" name="edit_handicap_type" id="edit_handicap_type">
										<option value="L">L</option>
										<option value="H">H</option>
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Nexr Of Kin<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_next_of_kin" id="edit_next_of_kin">
								</div>
							</div>  
							<input type="hidden" name="edit_id" id="edit_id">

							<div class="m-t-20 text-center">
								<button type="submit" name="doEdit" value="doEdit" class="btn btn-primary">Update Employee Detail</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div> 
	<div id="delete_employee_detail" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modal-md">
				<div class="modal-header">
					<h4 class="modal-title">Delete Employee Detail</h4>
				</div>
				<form action="<?php echo base_url('employee/delete');?>" method="post">
					<div class="modal-body card-box">
						<p>Are you sure want to delete this?</p>
						<input type="hidden" name="delete_id" id="delete_id" value="">
					<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<button type="submit" name="doDelete" value="doDelete"  class="btn btn-danger">Delete</button>
					</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 


<?php ?>

<script type="text/javascript">
	
 	

function showEdit(id){
 	var bs_url='<?php echo base_url('employee/getEmployeeDetails') ?>';
 	
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) { 
	    console.log(respo);
		$('#edit_id').val(respo.data.id);  
		$('#edit_employee_code').val(respo.data.employee_code);  
		$('#edit_gender_id').val(respo.data.gender_id).trigger('change');
		$('#edit_name').val(respo.data.name);
		$('#edit_relation').val(respo.data.relation).trigger('change');
		$('#edit_relation_name').val(respo.data.relation_name);
		$('#edit_phone_number').val(respo.data.phone_number);
		$('#edit_mobile_number').val(respo.data.mobile_number);
		$('#edit_aadhar_number').val(respo.data.adhar_number);
		$('#edit_pan_card_number').val(respo.data.pan_card_number);
		$('#edit_birth_date').val(respo.data.birth_date);
		$('#edit_joining_date').val(respo.data.joining_date);
		$('#edit_releaving_date').val(respo.data.releaving_date);
		$('#edit_address').val(respo.data.address);


		$('#edit_country_id').val(respo.data.country_id).trigger('change');
		// setStateById(respo.data.states);
		$('#edit_state_id').val(respo.data.state_id).trigger('change'); 
		$('#edit_city_id').val(respo.data.city_id).trigger('change');



		$('#edit_bank_id').val(respo.data.bank_id).trigger('change');		 
		$('#edit_bank_branch_id').val(respo.data.bank_branch_id).trigger('change');		 
		$('#edit_account_number').val(respo.data.account_number);
		$('#edit_ifsc_code').val(respo.data.ifsc_code);
		$('#edit_esi_reg_number').val(respo.data.esi_reg_number);
		$('#edit_pf_reg_number').val(respo.data.pf_reg_number);
		$('#edit_uan').val(respo.data.uan);
		$('#edit_qualification_id').val(respo.data.qualification_id);
		$('#edit_employee_type').val(respo.data.employee_type);
		$('#edit_department_id').val(respo.data.department_id).trigger('change');
		$('#edit_designation_id').val(respo.data.designation_id).trigger('change');
		$('#edit_email_id').val(respo.data.email_id);
		$('#edit_marital_status_id').val(respo.data.marital_status_id).trigger('change');
		$('#edit_nationality').val(respo.data.nationality);
		$('#edit_date_of_epf').val(respo.data.date_of_epf);
		$('#edit_date_of_pension').val(respo.data.date_of_pension); 
		if(respo.data.is_handicap == 0){
			$('#edit_is_handicap_yes').removeAttr('checked');
			$('#edit_is_handicap_no').attr('checked', 'checked');
			$('#edit_handicap_type_div').hide();
		}else if(respo.data.is_handicap == 1){
			$('#edit_is_handicap_no').removeAttr('checked');
			$('#edit_is_handicap_yes').attr('checked', 'checked');
			$('#edit_handicap_type_div').show();
			$('#edit_handicap_type').val(respo.data.handicap_type);
		}
		$('#edit_next_of_kin').val(respo.data.next_of_kin); 
		$("#edit_employee_details").modal('show');
	});
	 
}	


function showDelete(id){
 	var bs_url='<?php echo base_url('employee/getEmployeeDetails') ?>';  
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) {  
		$('#delete_id').val(respo.data.id);
		$("#delete_employee_detail").modal('show');
	});
	 
}		


function setStateById(states) {
      
   $("#state_id,#edit_state_id").append('<option value="" disabled selected>Select State</option>'), $.each(states, function(e, states) {
       $("#state_id,#edit_state_id").append($("<option></option>").attr("value", states.id).text(states.name))
   })
  
}


function getCityByStateID(id) {
	var e = id,
       t = $("#city_url").val();
   $("#city_id,#edit_city_id").empty(), $.ajax({
       type: "POST",
       url: t,
       data: {
           state_id: e
       },
       success: function(e) {
           var t = JSON.parse(e); 

           $("#city_id,#edit_city_id").append('<option value="" disabled selected>Select State</option>'), $.each(t, function(e, t) {
               $("#city_id,#edit_city_id").append($("<option></option>").attr("value", t.id).text(t.name))
           })
       }
   })
}

</script>