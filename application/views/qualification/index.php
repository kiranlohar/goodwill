<?php ?>
     <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-4">
							<h4 class="page-title">Qualification</h4>
						</div>
						<div class="col-xs-8 text-right m-b-30">
							<a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#add_qualification"><i class="fa fa-plus"></i> Add Qualification</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table datatable">
									<thead>
										<tr>
											<th>#Qualification ID</th>
											<th style="width:30%;">Qualification</th> 
											<th >Qualification Code</th> 
											<!-- <th>Created By</th>  -->
											<th>Created Date</th> 
											<th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($qualifications as $qual) { ?>
										<tr> 
											<td><?php echo $qual['id'];?></td>
											<td><?php echo $qual['qualification'];?></td><td><?php echo $qual['qualification_code'];?></td>
											<td><?php echo $qual['created_at'];?></td>
											<td class="text-right">
												<div class="dropdown">
													<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" onclick="showEdit(<?php echo $qual['id']; ?>)"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
														<li><a href="#" onclick="showDelete(<?php echo $qual['id']; ?>)"><i class="fa fa-trash-o m-r-5"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
									<?php } ?>										 
										 
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
    </div>
    <div id="add_qualification" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Add Qualification</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('qualification/add');?>" method="post">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label">Qualification<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="qualification">
								</div>

							<div class="form-group">
									<label class="control-label">Qualification code<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="qualification_code" id="qualification_code">
								</div>
							</div>  
						</div> 
						<div class="m-t-20 text-center">
							<button type="submit" name="doSubmit" value="doSubmit" class="btn btn-primary">Add Qualification</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="edit_qualification" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h4 class="modal-title text-center">Edit qualification</h4>
				</div>
				<div class="modal-body">
					<form class="m-b-30" action="<?php echo base_url('qualification/edit');?>" method="post">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label">qualification<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_qualification_name" id="edit_qualification_name" value="">
									<input class="form-control" type="hidden" name="edit_id" id="edit_id" value="">
								</div>

								<div class="form-group">
									<label class="control-label">Qualification code<span class="text-danger">*</span></label>
									<input class="form-control" type="text" name="edit_qualification_code" id="edit_qualification_code">
								</div>
							</div>  
						</div> 
						<div class="m-t-20 text-center">
							<button type="submit" name="doEdit" value="doEdit" class="btn btn-primary">Update qualification</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="delete_qualification" class="modal custom-modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modal-md">
				<div class="modal-header">
					<h4 class="modal-title">Delete Qualification</h4>
				</div>
				<form action="<?php echo base_url('qualification/delete');?>" method="post">
					<div class="modal-body card-box">
						<p>Are you sure want to delete this?</p>
						<input type="hidden" name="delete_id" id="delete_id" value="">
					<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<button type="submit" name="doDelete" value="doDelete"  class="btn btn-danger">Delete</button>
					</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 


<?php ?>



<script type="text/javascript">
	
function showEdit(id){
 	var bs_url='<?php echo base_url('qualification/getQualification') ?>';
 	
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) { 
		$('#edit_qualification_name').val(respo.data.qualification);
		$('#edit_qualification_code').val(respo.data.qualification_code);
		$('#edit_id').val(respo.data.id);
		$("#edit_qualification").modal('show');
	});
	 
}	


function showDelete(id){
 	console.log(id);
 	var bs_url='<?php echo base_url('qualification/getQualification') ?>';
 	var bs_url='<?php echo base_url('qualification/getQualification') ?>';
	$.ajax({
		url: bs_url,
		type: 'POST',
		dataType: 'json',
		data: {id: id},
	})
	.done(function(respo) {  
		console.log(respo);
		$('#delete_id').val(respo.data.id);
		$("#delete_qualification").modal('show');
	});
	 
}		

</script>